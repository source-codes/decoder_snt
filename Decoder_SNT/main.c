#include <msp430.h> 
#include "nrf905.h"
#include "nrf905_timer.h"
#include "cd4094.h"
#define RXADDR 0xE7E7E7E7 // Address of this device
#define TXADDR 0xE7E7E7E7 // Address of device to send to
#define TestSequenceNum             0

#define PACKET_NONE     0
#define PACKET_OK       1
#define PACKET_INVALID  2

static volatile uint32_t milliseconds;

static volatile uint8_t packetStatus;


void NRF905_CB_RXCOMPLETE(void)
{
    packetStatus = PACKET_OK;
    nRF905_standby();
}

void NRF905_CB_RXINVALID(void)
{
    packetStatus = PACKET_INVALID;
    nRF905_standby();
}

uint32_t millis(void)
{
    uint32_t ms;
    {
        ms = milliseconds;
    }
    return ms;
}

int main(void)
{
    WDTCTL = WDTPW | WDTHOLD;   // stop watchdog timer
//    P1DIR |= BIT0;                  // Initialize LED at P1.0

    DCOCTL = DCO0 + DCO1 + DCO2;              // Max DCO
    BCSCTL1 = RSEL0 + RSEL1 + RSEL2;          // Max RSEL
//    BCSCTL2 |= 0xC0;                          // ACLK = XT1
//    P5DIR |= 0x70;                            // P5.6,5,4 outputs
//    P5SEL |= 0x70;
    portInit_shiftreg();
    timer_init();
    triggerRelays(0x00);

    ANDgate_HIGH();

    WDI_TOGGLE();

    delay_msec(5);
    WDI_TOGGLE();

    // Start up
    nRF905_init();
    WDI_TOGGLE();

    // Set address of this device
    nRF905_setListenAddress(RXADDR);
    WDI_TOGGLE();

    nRF905_RX();
    uint8_t counter = 0;
    uint32_t sent = 0;
    uint32_t replies = 0;
    uint32_t timeouts = 0;
    uint32_t invalids = 0;
    WDI_TOGGLE();
    nRF905_SERVICE();
//    P1OUT |= 0x01;
//    while (1)
//    {
//        WDI_TOGGLE();
//        // Make data
//        char data[NRF905_MAX_PAYLOAD] = {0};
////        sprintf_P(data, PSTR("test %hhu"), counter);
//        counter++;
//
//        packetStatus = PACKET_NONE;
//
////        printf_P(PSTR("Sending data: %s\n"), data);
//
////        uint32_t startTime = x1;
//
//        // Send the data (send fails if other transmissions are going on, keep trying until success) and enter RX mode on completion
//        while(!nRF905_TX(TXADDR, data, sizeof(data), NRF905_NEXTMODE_RX));
//        sent++;
//        WDI_TOGGLE();
//
////        puts_P(PSTR("Data sent, waiting for reply..."));
//
//        uint8_t success;
//
//        // Wait for reply with timeout
//        uint32_t sendStartTime = x1;
//        while(1)
//        {
//            success = packetStatus;
//            if(success != PACKET_NONE)
//                break;
//            else if(x1 - sendStartTime > TIMEOUT)
//                break;
//        }
//        WDI_TOGGLE();
//
//        if(success == PACKET_NONE)
//        {
////            puts_P(PSTR("Ping timed out"));
//            timeouts++;
//        }
//        else if(success == PACKET_INVALID)
//        {
//            // Got a corrupted packet
////            puts_P(PSTR("Invalid packet!"));
//            invalids++;
//        }
//        else
//        {
//            // If success toggle LED and send ping time over UART
////            uint16_t totalTime = millis() - startTime;
//            P1OUT ^= 0x01;                          // Toggle P1.0 using exclusive-OR
//
//            replies++;
//
////            printf_P(PSTR("Ping time: %ums\n"), totalTime);
//            WDI_TOGGLE();
//
//            // Get the ping data
//            uint8_t replyData[NRF905_MAX_PAYLOAD];
//            nRF905_read(replyData, sizeof(replyData));
//            WDI_TOGGLE();
//            uint8_t rel = 0;
//            uint32_t num = 0;
//            uint8_t rel1 = 0;
//            for(rel = 0; rel <NRF905_MAX_PAYLOAD; rel++)
//            {
//                if(replyData[rel] == 0x01)
//                {
//                    rel1 = rel+1;
//                    switch(rel1)
//                    {
//                      case 1: num += 0x000001;     break;
//                      case 2: num += 0x000002;      break;
//                      case 3: num += 0x000004;      break;
//                      case 4: num += 0x000008;     break;
//                      case 5: num += 0x000010;     break;
//                      case 6: num += 0x000020;     break;
//                      case 7: num += 0x000040;     break;
//                      case 8: num += 0x000080;     break;
//                      case 9: num += 0x000100;     break;
//                      case 10: num += 0x000200;     break;
//                      case 11: num += 0x000400;     break;
//                      case 12: num += 0x000800;     break;
//                      case 13: num += 0x001000;     break;
//                      case 14: num += 0x002000;     break;
//                      case 15: num += 0x004000;     break;
//                      case 16: num += 0x008000;     break;
//                      case 17: num += 0x010000;     break;
//                      case 18: num += 0x020000;     break;
//                      case 19: num += 0x040000;     break;
//                      case 20: num += 0x080000;     break;
//                      case 21: num += 0x100000;     break;
//                      default:
//                               break;
//                    }
//                }
//            }
//            triggerRelays(num);
//
//
//            // Print out ping contents
////            printf_P(PSTR("Data from server: "));
////            for(uint8_t i=0;i<sizeof(replyData);i++)
////                printf_P(PSTR("%c"), replyData[i]);
////            puts_P(PSTR(""));
//        }
//
////        printf_P(PSTR("Totals: %lu Sent, %lu Replies, %lu Timeouts, %lu Invalid\n------\n"), sent, replies, timeouts, invalids);
//        WDI_TOGGLE();
//
//         delay_msec(100);
//         P1OUT ^= 0x01;
//         WDI_TOGGLE();
//    }

    uint32_t x2 = x1;
//    uint8_t temp123 = 0;
    while(1)
    {
        nRF905_SERVICE();
//        if(temp123 == 0)
//        {
//            RELAY2_LOW();
//            RELAY3_LOW();
//            temp123 = 1;
//        }
//        else if(temp123 == 1)
//        {
//            RELAY2_LOW();
//            RELAY3_LOW();
//            temp123 = 0;
//        }
        WDI_TOGGLE();

        while(packetStatus == PACKET_NONE){
            nRF905_SERVICE();

            P2OUT &= ~BIT3;
//            delay_msec(50);
//            P1OUT &= ~BIT4;
            WDI_TOGGLE();
//            delay_msec(50);
//            writeToShiftRegister(0x00000000);
//            delay_msec(50);
//            writeToShiftRegister(RELAY08);
        }
        WDI_TOGGLE();

        if(packetStatus != PACKET_OK)
        {
            WDI_TOGGLE();
//            P2OUT &= ~BIT3;
//            P1OUT &= ~BIT4;
            delay_msec(5);

            // Got a corrupted packet
            invalids++;
            packetStatus = PACKET_NONE;
            nRF905_RX();
        }
        else
        {
            WDI_TOGGLE();
//            pings++;
            packetStatus = PACKET_NONE;

            // Make buffer for data
            uint8_t buffer[NRF905_MAX_PAYLOAD];
            nRF905_read(buffer, sizeof(buffer));
//            uint8_t buffer[] = {0xFF, 0xFF, 0xFF, 0xFF, 0, 0, 0};
//            P2OUT |= BIT3;
//            P1OUT |= BIT4;


//            if(x1 - x2 > 250)
//            {
            uint32_t num1 = 0x00000000;

            if(buffer[1] == 0xFF && buffer[3] == 0xFF)
            {
                P2OUT |= BIT3;
                if(buffer[2] == 0xFF)
                {
//                    P1OUT |= BIT4;
                    num1 |= RELAY04;

                }

                else if(buffer[2] == 0x00)
                {
//                    P1OUT &= ~BIT4;
                    num1 &= ~RELAY04;

                }
                if(buffer[6] == 0xFF)
                {
//                    P2OUT |= BIT3;
                    num1 |= RELAY08;
                }
                else if(buffer[6] == 0x00)
                {
//                    P2OUT &= ~BIT3;
                    num1 &= ~RELAY08;
                }

                writeToShiftRegister(num1);
                delay_msec(5);
            }
//                x2 = x1;
//            }
//            triggerRelays(num1);

//            uint8_t rel = 0;
//            uint32_t num = 0;
//            uint8_t rel1 = 0;
//            for(rel = 0; rel <NRF905_MAX_PAYLOAD; rel++)
//            {
//                if(buffer[rel] == 0x01)
//                {
//                    rel1 = rel+1;
//                    switch(rel1)
//                    {
//                      case 1: num += 0x000001;     break;
//                      case 2: num += 0x000002;      break;
//                      case 3: num += 0x000004;      break;
//                      case 4: num += 0x000008;     break;
//                      case 5: num += 0x000010;     break;
//                      case 6: num += 0x000020;     break;
//                      case 7: num += 0x000040;     break;
//                      case 8: num += 0x000080;     break;
//                      case 9: num += 0x000100;     break;
//                      case 10: num += 0x000200;     break;
//                      case 11: num += 0x000400;     break;
//                      case 12: num += 0x000800;     break;
//                      case 13: num += 0x001000;     break;
//                      case 14: num += 0x002000;     break;
//                      case 15: num += 0x004000;     break;
//                      case 16: num += 0x008000;     break;
//                      case 17: num += 0x010000;     break;
//                      case 18: num += 0x020000;     break;
//                      case 19: num += 0x040000;     break;
//                      case 20: num += 0x080000;     break;
//                      case 21: num += 0x100000;     break;
//                      default:
//                               break;
//                    }
//                }
//            }
//            WDI_TOGGLE();

//            triggerRelays(num);

            nRF905_RX();
//            // Send payload (send fails if other transmissions are going on, keep trying until success) and enter RX once complete
//            while(!nRF905_TX(TXADDR, buffer, sizeof(buffer), NRF905_NEXTMODE_RX)){
                WDI_TOGGLE();

//        }
       }

    }
}



