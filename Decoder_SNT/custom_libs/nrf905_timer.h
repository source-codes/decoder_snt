#ifndef CUSTOM_LIBS_NRF905_TIMER_H_
#define CUSTOM_LIBS_NRF905_TIMER_H_


#include <msp430.h>
#include <stdint.h>

#define TIMEOUT 1000 // ~1 second ping timeout
#define const123        1000

extern volatile uint32_t x1;


void timer_init(void);
void delay_msec(uint32_t val);

__interrupt void Timer_A (void);

#endif /* CUSTOM_LIBS_NRF905_TIMER_H_ */
