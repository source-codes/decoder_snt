#ifndef CUSTOM_LIBS_NRF905_DEFS_H_
#define CUSTOM_LIBS_NRF905_DEFS_H_

// Instructions
#define NRF905_CMD_NOP          0xFF
#define NRF905_CMD_W_CONFIG     0x00
#define NRF905_CMD_R_CONFIG     0x10
#define NRF905_CMD_W_TX_PAYLOAD 0x20
#define NRF905_CMD_R_TX_PAYLOAD 0x21
#define NRF905_CMD_W_TX_ADDRESS 0x22
#define NRF905_CMD_R_TX_ADDRESS 0x23
#define NRF905_CMD_R_RX_PAYLOAD 0x24
#define NRF905_CMD_CHAN_CONFIG  0x80


// Registers
#define NRF905_REG_CHANNEL          0x00
#define NRF905_REG_CONFIG1          0x01
#define NRF905_REG_ADDR_WIDTH       0x02
#define NRF905_REG_RX_PAYLOAD_SIZE  0x03
#define NRF905_REG_TX_PAYLOAD_SIZE  0x04
#define NRF905_REG_RX_ADDRESS       0x05
#define NRF905_REG_CONFIG2          0x09

// TODO remove
#define NRF905_REG_AUTO_RETRAN  NRF905_REG_CONFIG1
#define NRF905_REG_LOW_RX       NRF905_REG_CONFIG1
#define NRF905_REG_PWR          NRF905_REG_CONFIG1
#define NRF905_REG_BAND         NRF905_REG_CONFIG1
#define NRF905_REG_CRC          NRF905_REG_CONFIG2
#define NRF905_REG_CLK          NRF905_REG_CONFIG2
#define NRF905_REG_OUTCLK       NRF905_REG_CONFIG2
#define NRF905_REG_OUTCLK_FREQ  NRF905_REG_CONFIG2


// Clock options
#define NRF905_CLK_4MHZ         0x00
#define NRF905_CLK_8MHZ         0x08
#define NRF905_CLK_12MHZ        0x10
#define NRF905_CLK_16MHZ        0x18
#define NRF905_CLK_20MHZ        0x20

// Register masks
#define NRF905_MASK_CHANNEL     0xFE
#define NRF905_MASK_AUTO_RETRAN ~(NRF905_AUTO_RETRAN_ENABLE | NRF905_AUTO_RETRAN_DISABLE) //0xDF
#define NRF905_MASK_LOW_RX      ~(NRF905_LOW_RX_ENABLE | NRF905_LOW_RX_DISABLE) //0xEF
#define NRF905_MASK_PWR         ~(NRF905_PWR_n10 | NRF905_PWR_n2 | NRF905_PWR_6 | NRF905_PWR_10) //0xF3
#define NRF905_MASK_BAND        ~(NRF905_BAND_433 | NRF905_BAND_868 | NRF905_BAND_915) //0xFD
#define NRF905_MASK_CRC         (uint8_t)(~(NRF905_CRC_DISABLE | NRF905_CRC_8 | NRF905_CRC_16)) //0x3F // typecast to stop compiler moaning about large integer truncation
#define NRF905_MASK_CLK         ~(NRF905_CLK_4MHZ | NRF905_CLK_8MHZ | NRF905_CLK_12MHZ | NRF905_CLK_16MHZ | NRF905_CLK_20MHZ) //0xC7
#define NRF905_MASK_OUTCLK      ~(NRF905_OUTCLK_DISABLE | NRF905_OUTCLK_4MHZ | NRF905_OUTCLK_2MHZ | NRF905_OUTCLK_1MHZ | NRF905_OUTCLK_500KHZ) // 0xF8


// Bit positions
#define NRF905_STATUS_DR        5
#define NRF905_STATUS_AM        7

#include "nrf905_config.h"

#if (NRF905_DR_SW && NRF905_INTERRUPTS)
    #error "NRF905_INTERRUPTS and NRF905_DR_SW cannot both be enabled"
#elif (NRF905_AM_SW && NRF905_INTERRUPTS_AM)
    #error "NRF905_INTERRUPTS_AM and NRF905_AM_SW cannot both be enabled"
#elif (!NRF905_INTERRUPTS && NRF905_INTERRUPTS_AM)
    #error "NRF905_INTERRUPTS_AM cannot be enabled without NRF905_INTERRUPTS"
#endif

#if DECODER_board_selected
// TX / RX mode pin
#define TX_EN_IOdirection           P5DIR
#define TX_EN_port                  P5OUT
#define TX_EN_pin                   BIT4

// Enable/standby pin
#define TRX_CE_IOdirection          P5DIR
#define TRX_CE_port                 P5OUT
#define TRX_CE_pin                  BIT5

// Power mode pin
#define PWR_UP_IOdirection          P5DIR
#define PWR_UP_port                 P5OUT
#define PWR_UP_pin                  BIT6

// Carrier detect pin (Optional, used for collision avoidance if NRF905_COLLISION_AVOID is 1 or if you want to use the nRF905_airwayBusy() function)
#define CD_IOdirection              P5DIR
#define CD_port                     P5IN
#define CD_pin                      BIT7

#if !NRF905_AM_SW
// Address match pin
// If using AM interrupt (NRF905_INTERRUPTS_AM 1) then this must be an external interrupt pin that matches the interrupt settings below.
// If NRF905_INTERRUPT_AM is 0 and NRF905_AM_SW is 1 then this pin does not need to be connected
#define AM_IOdirection              P4DIR
#define AM_port                     P4IN
#define AM_pin                      BIT7
#endif

#if !NRF905_DR_SW
// Data ready pin
// If using interrupts (NRF905_INTERRUPTS 1) then this must be an external interrupt pin that matches the interrupt settings below.
// If NRF905_INTERRUPTS is 0 and NRF905_DR_SW is 1 then this pin does not need to be connected
#define DR_IOdirection              P4DIR
#define DR_port                     P4IN
#define DR_pin                      BIT6
#endif

// SPI slave select pin
#define CSN_IOdirection             P5DIR
#define CSN_port                    P5OUT
#define CSN_pin                     BIT0

#elif ENCODER_board_selected
// TX / RX mode pin
#define TX_EN_IOdirection           P5DIR
#define TX_EN_port                  P5OUT
#define TX_EN_pin                   BIT5

// Enable/standby pin
#define TRX_CE_IOdirection          P5DIR
#define TRX_CE_port                 P5OUT
#define TRX_CE_pin                  BIT4

// Power mode pin
#define PWR_UP_IOdirection          P5DIR
#define PWR_UP_port                 P5OUT
#define PWR_UP_pin                  BIT6

// Carrier detect pin (Optional, used for collision avoidance if NRF905_COLLISION_AVOID is 1 or if you want to use the nRF905_airwayBusy() function)
#define CD_IOdirection              P5DIR
#define CD_port                     P5IN
#define CD_pin                      BIT7

#if !NRF905_AM_SW
// Address match pin
// If using AM interrupt (NRF905_INTERRUPTS_AM 1) then this must be an external interrupt pin that matches the interrupt settings below.
// If NRF905_INTERRUPT_AM is 0 and NRF905_AM_SW is 1 then this pin does not need to be connected
#define AM_IOdirection              P4DIR
#define AM_port                     P4IN
#define AM_pin                      BIT7
#endif

#if !NRF905_DR_SW
// Data ready pin
// If using interrupts (NRF905_INTERRUPTS 1) then this must be an external interrupt pin that matches the interrupt settings below.
// If NRF905_INTERRUPTS is 0 and NRF905_DR_SW is 1 then this pin does not need to be connected
#define DR_IOdirection              P4DIR
#define DR_port                     P4IN
#define DR_pin                      BIT6
#endif

// SPI slave select pin
#define CSN_IOdirection             P5DIR
#define CSN_port                    P5OUT
#define CSN_pin                     BIT0


#endif




#endif /* CUSTOM_LIBS_NRF905_DEFS_H_ */
